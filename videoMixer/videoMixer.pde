import controlP5.*;
import processing.video.*;
import java.util.*;

boolean playerFullScreen = false;
int playerOnMonitor = 1;

// --------------------------
final int GEN_BLEND = 1;
final int GEN_COLOR = 4;
final int GEN_POS = 5;
final int GEN_TYPE = 6;
final int VID_TOP_BLEND = 21;
final int VID_TOP_LOAD = 22;
final int VID_BOT_BLEND = 31;
final int VID_BOT_LOAD = 32;
final int BG_COLOR = 51;
final int BG_IMG = 52;
final int MAPPING_CALIBRATE = 70;
final int MAPPING_SAVE = 71;
final int MAPPING_LOAD = 72;

// Thumbnail size
final int thumbWidth = 96;
final int thumbHeight = 54;

// --------------------------
float v_opacity, v_size, v_hue, v_saturation, v_lightness;
ControlP5 cp5;
Slider2D xy;
VideoPlayer videoPlayer;
PFont fonts[] = new PFont[4];
int gray = 0xffcccccc;
int y = -6, x = 4;
boolean started = false;

// Layer Thumbnails
volatile PImage thumbs[] = new PImage[4];

// --------------------------
void settings() {
  size(400, 740, P3D);
  videoPlayer = new VideoPlayer(dataPath(""), playerFullScreen, playerOnMonitor);
}

// -------------
void loadFonts() {
  fonts[0] = createFont("Assistant-Light.ttf", 12);
  fonts[3] = createFont("Assistant-Bold.ttf", 14);
}

// --------------------------
void setup() {
  surface.setLocation(10, 10);
  surface.setTitle("sketch_video_mixer_gui");
  
  noStroke();
  fill(60);
  frameRate(30);

  for (int i=0; i<4; i++) {
    thumbs[i] = createImage(thumbWidth, thumbHeight, RGB);
  }

  loadFonts();

  cp5 = new ControlP5(this);

  // ------------- GENERATIVE
  addLabel("gen_label", "Generative");
  addBlendMode("gen_blend", "blend mode", GEN_BLEND);
  addSlider("gen_opacity", "opacity", 0.5, 0, 1.0); 
  addSlider("gen_size", "size", 1.0, 0, 3);
  addSlider("gen_sides", "sides", 0.0, 0, 1);
  addSlider("gen_distortion", "distortion", 0.5, 0, 1);
  addSlider("gen_speed", "speed", 0.5, 0, 1);
  addSlider("gen_width", "width", 0.0, 0, 1);
  addColorWheel("gen_color");
  addGenType("gen_type", "type", GEN_TYPE);
  addGenPos("gen_pos", "position", GEN_POS);

  y -= 3;
  
  // -------------VIDEO TOP
  addLabel("top_label", "Video: top");
  addLoad("top_load_video", "load", VID_TOP_LOAD);
  addBlendMode("top_blend", "blend mode", VID_TOP_BLEND);
  addSlider("top_opacity", "opacity", 0.0, 0, 1); 
  addSlider("top_size", "size", 1.0, 1, 3);
  addSlider("top_hue", "hue", 0.5, 0, 1);
  addSlider("top_saturation", "saturation", 0.5, 0, 1);
  addSlider("top_brightness", "brightness", 0.5, 0, 1);

  y += 6;

  // -------------
  addLabel("bot_label", "Video: bottom");
  addLoad("bot_load_video", "load", VID_BOT_LOAD);
  addBlendMode("bot_blend", "blend mode", VID_BOT_BLEND);
  addSlider("bot_opacity", "opacity", 0.0, 0, 1); 
  addSlider("bot_size", "size", 1.0, 1, 3);
  addSlider("bot_hue", "hue", 0.5, 0, 1);
  addSlider("bot_saturation", "saturation", 0.5, 0, 1);
  addSlider("bot_brightness", "brightness", 0.5, 0, 1);

  y += 6;

  // -------------
  addLabel("bg_label", "Background");
  addLoad("gen_load_img", "load", BG_IMG);
  addSlider("bg_size", "size", 1.0, 1, 3);
  addSlider("bg_tint", "tint", 1.0, 0, 1);
  y += 1;
  addColorWheel("bg_color");

  // -------------
  addLabel("map_label", "Mapping");
  y += 10;
  addBang("map_calibrate", "callibrate", MAPPING_CALIBRATE, false);
  addBang("map_save", "save", MAPPING_SAVE, false);
  addBang("map_load", "load", MAPPING_LOAD, true);

  started = true;
}

// -------------
void addColorWheel(String id) {
  Controller comp = cp5.addColorWheel(id, x, y += 10, 160)
    .plugTo(videoPlayer, id)
    .setRGB(#000000)
    .setLabelVisible(false);
  y += comp.getHeight();
}

// -------------
void addLoad(String id, String name, int numId) {
  cp5.addButton(id)
    .setId(numId)
    .setFont(fonts[0])
    .setLabel(name)
    .setPosition(300, y + 93)
    .setSize(96, 15);
}

// -------------
void addBang(String id, String name, int numId, boolean linebreak) {
  Controller comp = cp5.addButton(id)
    .setId(numId)
    .setFont(fonts[0])
    .setLabel(name)
    .setPosition(x, y)
    .setSize(96, 15);
  if (linebreak) {
    x = 10;
    y += comp.getHeight();
  } else {
    x += comp.getWidth() + 5;
  }
}

// -------------
void addSlider(String id, String name, float val, float min, float max) {
  Controller comp = cp5.addSlider(id)
    .plugTo(videoPlayer, id)
    .setRange(min, max)
    .setValue(val)
    .setPosition(x, y += 10)
    .setSize(200, 15)
    .setFont(fonts[0])
    .setLabel(name);
  y += comp.getHeight() * 0.5;
}

// -------------
void addLabel(String id, String name) {
  Textlabel comp = cp5.addTextlabel(id)
    .setText(name)
    .setPosition(196, y + 8)
    .setColorValue(0xffcccccc)
    .setFont(fonts[3]);
  comp.get().alignX(ControlP5.RIGHT);
}

// -------------
void addBlendMode(String id, String name, int numId) {
  RadioButton comp = cp5.addRadioButton(id)
    .setPosition(x, y += 10)
    .setSize(20, 15)
    .setId(numId)
    .setLabel(name)
    .setItemsPerRow(3)
    .setSpacingColumn(70)
    .addItem(id + "blend", BLEND)
    .addItem(id + "add", ADD)
    .addItem(id + "subtract", SUBTRACT);
  comp.getItems().get(0).setValue(true);
  for (Toggle t : comp.getItems()) {
    t.setLabel(t.getLabel().substring(id.length()));
    t.setFont(fonts[0]);
  }

  y += comp.getHeight();
}

// -------------
void addGenType(String id, String name, int numId) {
  RadioButton comp = cp5.addRadioButton(id)
    .setPosition(183, y - 160)
    .setSize(20, 15)
    .setId(numId)
    .setLabel(name)
    .addItem(id + "radial", 0)
    .addItem(id + "grid", 1);
  comp.getItems().get(0).setValue(true);
  for (Toggle t : comp.getItems()) {
    t.setLabel(t.getLabel().substring(id.length()));
    t.setFont(fonts[0]);
  }
}

// -------------
void addGenPos(String id, String name, int numId) {
  xy = cp5.addSlider2D(id)
    .setDecimalPrecision(2)
    .setPosition(300, y - 190)
    .setId(numId)
    .setLabel(name)
    .setSize(thumbWidth, thumbHeight)
    .setFont(fonts[0])
    .setLabelVisible(false)
    .setMinMax(-0.2, -0.2, 1.2, 1.2)
    .setValue(0.5, 0.5);
}

// --------------------------
void draw() {
  background(40);
  int heights[] = new int[]{285, 108, 108, 201};
  int y = 2;
  for (int i=0; i<4; i++) {
    fill(i % 2 == 0 ? #393C3E : #4D4D49);
    rect(2, y, width-4, heights[i]);
    image(thumbs[3-i], 300, y + 30);
    y += heights[i] + 2;
  }
}

// -------------
void controlEvent(ControlEvent e) {
  float fval = e.getValue();
  int ival = (int)fval;

  switch(e.getId()) {

  case GEN_BLEND: 
    videoPlayer.gen_blend = ival;
    break;

  case GEN_POS:
    videoPlayer.gen_pos.set(
      e.getArrayValue()[0], 
      e.getArrayValue()[1]);
    break;

  case GEN_TYPE:
    videoPlayer.gen_type = ival;
    break;

  case VID_TOP_BLEND:
    videoPlayer.top_blend = ival;
    break;

  case VID_TOP_LOAD:
    selectInput("Select top video", "topSelected");
    break;

  case VID_BOT_BLEND:
    videoPlayer.bot_blend = ival;
    break;

  case VID_BOT_LOAD:
    selectInput("Select bottom video", "botSelected");
    break;

  case BG_IMG:
    selectInput("Select background image", "bgSelected");
    break;

  case MAPPING_CALIBRATE:
    videoPlayer.mappingCalibrate();
    break;

  case MAPPING_SAVE:
    videoPlayer.mappingSave();
    break;

  case MAPPING_LOAD:
    videoPlayer.mappingLoad();
    break;
  }
}

// -------------
public void bgSelected(File f) {
  if (f != null) {
    videoPlayer.loadBackgroundImage(f.getAbsolutePath());
  }
}

// -------------
public void topSelected(File f) {
  if (f != null) {
    videoPlayer.loadTopVideo(f.getAbsolutePath());
  }
}

// -------------
public void botSelected(File f) {
  if (f != null) {
    videoPlayer.loadBotVideo(f.getAbsolutePath());
  }
}
