import deadpixel.keystone.*;

class VideoPlayer extends PApplet {
  boolean fullscreen;
  int monitor;
  int renderWidth = 960;
  int renderHeight = 540;
  
  int windowWidth;
  int windowHeight;
  
  String path;
  PGraphics final_mix;
  PGraphics layers[] = new PGraphics[4];
  PGraphics thumbLayers[] = new PGraphics[4];
  PShader hsb_shader;
  PShader gen_shader;

  // For projection mapping
  Keystone ks;
  CornerPinSurface mappedSurface;

  // --------------------------
  // GENERATIVE
  int gen_blend = BLEND;
  float gen_opacity = 1;
  Interpolator gen_opacity_i = new Interpolator(gen_opacity);  
  float gen_size = 1;
  Interpolator gen_size_i = new Interpolator(gen_size);  
  int gen_color = #000000;
  float gen_sides = 3;
  Interpolator gen_sides_i = new Interpolator(gen_sides);
  float gen_distortion = 0.0;
  Interpolator gen_distortion_i = new Interpolator(gen_distortion);
  float gen_speed = 0.0;
  float gen_time = 0.0;
  float gen_width = 0.0;
  Interpolator gen_width_i = new Interpolator(gen_width);  
  PVector gen_pos = new PVector(0.5, 0.5);
  Interpolator gen_posx_i = new Interpolator(gen_pos.x);
  Interpolator gen_posy_i = new Interpolator(gen_pos.y);
  int gen_type = 0;

  // --------------------------
  // TOP VIDEO
  int top_blend = BLEND;
  float top_opacity = 0;
  Interpolator top_opacity_i = new Interpolator(top_opacity);
  float top_size = 1;
  Interpolator top_size_i = new Interpolator(top_size);
  float top_hue = 0.5;
  float top_saturation = 0.5;
  float top_brightness = 0.5;
  Movie top_movie;
  String top_file_path = null;

  // --------------------------
  // BOTTOM VIDEO
  int bot_blend = BLEND;
  float bot_opacity = 0;
  Interpolator bot_opacity_i = new Interpolator(bot_opacity);
  float bot_size = 1;
  Interpolator bot_size_i = new Interpolator(bot_size);
  float bot_hue = 0.5;
  float bot_saturation = 0.5;
  float bot_brightness = 0.5;
  Movie bot_movie;
  String bot_file_path = null;

  // --------------------------
  // BACKGROUND
  int bg_color = #000000;
  float bg_size = 1;
  Interpolator bg_size_i = new Interpolator(bg_size);
  float bg_tint = 1;
  PImage bg_img;
  String bg_file_path = null;

  // --------------------------
  VideoPlayer(String path, boolean fullscreen, int monitor) {   
    this.fullscreen = fullscreen;
    this.path = path;
    this.monitor = monitor;

    PApplet.runSketch(new String[]{ this.getClass().getName() }, this);
  }

  // --------------------------
  void settings() {
    if (fullscreen) {
      windowWidth = displayWidth;
      windowHeight = displayHeight;
      fullScreen(P3D, monitor);
    } else {
      windowWidth = renderWidth;
      windowHeight = renderHeight;
      size(windowWidth, windowHeight, P3D);
    }
  }

  // --------------------------
  void setup() {
    surface.setTitle("sketch_video_player");

    loadShaders();
    frameRate(30);

    ks = new Keystone(this);
    mappedSurface = ks.createCornerPinSurface(windowWidth, windowHeight, 8);

    final_mix = createGraphics(windowWidth, windowHeight, P3D);

    bg_img = createImage(renderWidth, renderHeight, RGB);

    for (int i=0; i<4; i++) {
      layers[i] = createGraphics(renderWidth, renderHeight, P3D);
      layers[i].beginDraw();
      layers[i].imageMode(CENTER);
      layers[i].clear();
      layers[i].noStroke();
      layers[i].endDraw();
    }

    for (int i=0; i<4; i++) {
      thumbLayers[i] = createGraphics(thumbWidth, thumbHeight, P3D);
      thumbLayers[i].beginDraw();
      thumbLayers[i].background(40);
      thumbLayers[i].endDraw();
    }
    //startVideos();
  }

  // --------------------------
  void startVideos() {
    top_movie = new Movie(this, path + "/a.mp4");
    top_movie.loop();

    bot_movie = new Movie(this, path + "/b.mp4");
    bot_movie.loop();
  }

  // --------------------------
  void draw() {
    if (!started) {
      return;
    }

    loadNewMedia();

    if (bot_movie != null && bot_movie.available()) {
      bot_movie.read();
      updateThumb(1, bot_movie, false);
    }

    if (top_movie != null && top_movie.available()) {
      top_movie.read();
      updateThumb(2, top_movie, false);
    }

    // --- BACKGROUND
    layers[0].beginDraw();
    layers[0].background(bg_color);
    layers[0].tint(255, 255 * (1-bg_tint));
    float sz = (float)bg_size_i.getNextForTarget(bg_size);
    layers[0].image(bg_img, renderWidth/2, renderHeight/2, 
      renderWidth * sz, renderHeight * sz);
    layers[0].endDraw();

    // --- BOTTOM VIDEO
    if (bot_movie != null) {
      hsb_shader.set("u_hue", bot_hue);
      hsb_shader.set("u_saturation", bot_saturation);
      hsb_shader.set("u_brightness", bot_brightness);
      hsb_shader.set("u_opacity", (float)bot_opacity_i.getNextForTarget(bot_opacity));

      layers[1].beginDraw();
      layers[1].clear();
      layers[1].shader(hsb_shader);
      sz = (float)bot_size_i.getNextForTarget(bot_size);
      layers[1].image(bot_movie, renderWidth/2, renderHeight/2, 
        renderWidth * sz, renderHeight * sz);
      layers[1].endDraw();
    }

    // --- TOP VIDEO
    if (top_movie != null) {
      hsb_shader.set("u_hue", top_hue);
      hsb_shader.set("u_saturation", top_saturation);
      hsb_shader.set("u_brightness", top_brightness);
      hsb_shader.set("u_opacity", (float)top_opacity_i.getNextForTarget(top_opacity));

      layers[2].beginDraw();
      layers[2].clear();
      layers[2].shader(hsb_shader);
      sz = (float)top_size_i.getNextForTarget(top_size);
      layers[2].image(top_movie, renderWidth/2, renderHeight/2, 
        renderWidth * sz, renderHeight * sz);
      layers[2].endDraw();
    }

    // --- GENERATIVE
    redrawGenerativeLayer();

    // draw all layers onto layer final_mix
    final_mix.beginDraw();
    final_mix.blendMode(BLEND);
    final_mix.image(layers[0], 0, 0, windowWidth, windowHeight); // BG
    final_mix.blendMode(bot_blend);
    final_mix.image(layers[1], 0, 0, windowWidth, windowHeight); // BOT
    final_mix.blendMode(top_blend);
    final_mix.image(layers[2], 0, 0, windowWidth, windowHeight); // TOP
    final_mix.blendMode(gen_blend);
    final_mix.image(layers[3], 0, 0, windowWidth, windowHeight); // GEN
    final_mix.endDraw();

    background(0);
    mappedSurface.render(final_mix);
  }

  // --------------------------
  void mousePressed() {     
    loadShaders();
  }

  // --------------------------
  void loadShaders() {
    hsb_shader = loadShader(path + "/hsb.frag");
    gen_shader = loadShader(path + "/gen.frag");
    gen_shader.set("u_resolution", (float)renderWidth, (float)renderHeight);
  }

  // --------------------------
  void loadNewMedia() {
    if (bg_file_path != null) {
      bg_img = loadImage(bg_file_path);
      updateThumb(0, bg_img, false);
      bg_file_path = null;
    }

    if (top_file_path != null) {
      if(top_movie != null) {
        top_movie.dispose();
      }
      top_movie = new Movie(this, top_file_path);
      top_movie.loop();
      top_movie.volume(0);
      top_file_path = null;
    }

    if (bot_file_path != null) {
      if(bot_movie != null) {
        bot_movie.dispose();
      }
      bot_movie = new Movie(this, bot_file_path);
      bot_movie.loop();
      bot_movie.volume(0);
      bot_file_path = null;
    }
  }

  // --------------------------
  void updateThumb(int which, PImage content, boolean wipe) {
    // half frame rate
    if (frameCount % 2 == 0) {
      return;
    }
    thumbLayers[which].beginDraw();
    if (wipe) {
      thumbLayers[which].clear();
    }
    thumbLayers[which].image(content, 0, 0, thumbWidth, thumbHeight);
    thumbLayers[which].endDraw();
    thumbs[which] = thumbLayers[which].get();
  }

  // --------------------------
  void redrawGenerativeLayer() {
    gen_shader.set("u_sides", (float)gen_sides_i.getNextForTarget(gen_sides));
    gen_shader.set("u_distortion", (float)gen_distortion_i.getNextForTarget(gen_distortion));
    gen_shader.set("u_time", gen_time += 0.1 * (gen_speed - 0.5));
    gen_shader.set("u_width", (float)gen_width_i.getNextForTarget(gen_width));
    gen_shader.set("u_size", (float)gen_size_i.getNextForTarget(gen_size));
    gen_shader.set("u_opacity", (float)gen_opacity_i.getNextForTarget(gen_opacity));
    gen_shader.set("u_pos", 
      (float)gen_posx_i.getNextForTarget(gen_pos.x), 
      (float)gen_posy_i.getNextForTarget(gen_pos.y));
    gen_shader.set("u_type", gen_type);

    layers[3].beginDraw();
    layers[3].shader(gen_shader);
    layers[3].clear();
    layers[3].fill(gen_color);
    layers[3].rect(0, 0, renderWidth, renderHeight);
    layers[3].endDraw();

    updateThumb(3, layers[3], true);
  }

  // --------------------------
  void loadBackgroundImage(String path) {
    bg_file_path = path;
  }

  // --------------------------
  void loadTopVideo(String path) {
    top_file_path = path;
  }

  // --------------------------
  void loadBotVideo(String path) {
    bot_file_path = path;
  }

  // --------------------------
  // Mapping
  void mappingCalibrate() {
    ks.toggleCalibration();
  }

  void mappingLoad() {
    ks.load(path + "/mapping");
  }

  void mappingSave() {
    ks.save(path + "/mapping");
  }
}
