double constrain(double value, double start, double stop) {
  if (value < start) {
    return start;
  }
  if (value > stop) {
    return stop;
  }
  return value;
}

double map(double value, double start1, double stop1, double start2, double stop2) {
  return stop1 == start1 ? value : start2 +
    (stop2 - start2) * ((value - start1) / (stop1 - start1));
}

/**
 * By Abe Pazos - (c) 2019 - GPL3
 * Interpolates values toward a target using physics.
 * It uses a maxSpeed, a maxAcceleration, and a damp distance.
 * Useful with midi knobs or other values if the target
 * value can change.
 */
class Interpolator {
  private double currentValue;
  private double currentSpeed;
  private double maxSpeed = 0.01;
  private double maxAcceleration = 0.0003;
  private double dampDist = 0.20;

  public Interpolator(double initialValue) {
    this.currentValue = initialValue;
  }

  public double getNextForTarget(double targetValue) {
    double offset = targetValue - currentValue;
    double d = Math.min(dampDist, Math.abs(offset));
    offset = (offset > 0 ? 1 : -1) * map(d, 0, dampDist, 0, maxSpeed);
    double acceleration = constrain(offset - currentSpeed, 
      -maxAcceleration, maxAcceleration);

    currentSpeed += acceleration;
    currentSpeed = constrain(currentSpeed, -maxSpeed, maxSpeed);

    currentValue += currentSpeed;

    return currentValue;
  }

  public void set(double target) {
    currentValue = target;
    currentSpeed = 0;
  }

  public void setSpeeds(double maxSpeed, 
    double maxAcceleration, 
    double dampDistance) {
    this.maxSpeed = maxSpeed;
    this.maxAcceleration = maxAcceleration;
    this.dampDist = dampDistance;
  }
}
