#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PI  3.14159
#define XI  (.5 * PI)
#define TAU (2. * PI)

varying vec4 vertColor;

uniform float u_sides;
uniform float u_distortion;
uniform float u_time;
uniform float u_width;
uniform float u_size;
uniform float u_opacity;
uniform vec2 u_pos;
uniform int u_type;
uniform vec2 u_resolution;


//	Simplex 3D Noise 
//	by Ian McEwan, Ashima Arts
//
vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r;}

float snoise(vec3 v){ 
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

  // First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

  // Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //  x0 = x0 - 0. + 0.0 * C 
  vec3 x1 = x0 - i1 + 1.0 * C.xxx;
  vec3 x2 = x0 - i2 + 2.0 * C.xxx;
  vec3 x3 = x0 - 1. + 3.0 * C.xxx;

  // Permutations
  i = mod(i, 289.0 ); 
  vec4 p = permute( permute( permute( 
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

  // Gradients
  // ( N*N points uniformly over a square, mapped onto an octahedron.)
  float n_ = 1.0/7.0; // N=7
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z *ns.z);  //  mod(p,N*N)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

  //Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

  // Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                dot(p2,x2), dot(p3,x3) ) );
}

// Based on https://www.shadertoy.com/view/XlsczS
float ngon(vec2 uv, float radius, float nsides){
  float angle = atan(uv.y, uv.x) + XI + u_time;
  float split = TAU / nsides;
  return length(uv) * cos(split * floor(.5 + angle / split) - angle) - radius +
    snoise(vec3(uv, u_time)) * (u_distortion);
}

// Could be used for rotating the radial shape or the grid
mat2 rot(float a) {
  return mat2(cos(a), sin(a), -sin(a), cos(a));
}

void main() {
  float shape;

  if(u_type == 0) {
    // radial shape
    vec2 uv = gl_FragCoord.xy / u_resolution.y;
    vec2 shape_uv = vec2(u_pos.x * u_resolution.x / u_resolution.y, 1.0 - u_pos.y);  

    shape = ngon(uv - shape_uv, u_size, 3.0 + floor(24.0 * u_sides));
    float a = 2.0 - 2.0 * clamp(u_width * 2.0, 0.0, 1.0);
    float b = 2.0 * clamp(u_width * 2.0, 1.0, 2.0) - 2.0;
    shape = smoothstep(-0.003-a, -a, shape) - 
            smoothstep( b, 0.003+b, shape);

    gl_FragColor = vec4(vertColor.rgb, shape * u_opacity);

  } else {
    // grid shape
    vec2 uv = gl_FragCoord.xy;

    uv.y -= (1.0 - u_pos.y) * u_resolution.y;
    uv.x -= u_pos.x * u_resolution.x;

    // distortion
    float distAmt = 100 * (u_distortion);
    float distDensity = 0.001 + u_sides * 0.05;
    uv.y += distAmt * snoise(vec3(uv * distDensity, u_time));
    uv.x += distAmt * snoise(vec3(u_time, uv * distDensity));

    // grid size
    float s = (8.0 + u_size * u_resolution.y * .3);

    // cente in grid cell
    float w = s * 0.5;

    // lines can be blurred
    float blur = 1.0 + 10.0 * u_width;

    vec2 pat = smoothstep(w-blur, w, mod(uv, s)) -
               smoothstep(w, w+blur, mod(uv, s));

    shape = pat.x + pat.y;

    gl_FragColor = vec4(vertColor.rgb, shape * u_opacity);    
  }
}

