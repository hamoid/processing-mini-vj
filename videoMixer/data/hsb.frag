#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

varying vec4 vertColor;
varying vec4 vertTexCoord;

uniform float u_hue;
uniform float u_saturation;
uniform float u_brightness;
uniform float u_opacity;

// form Processing
uniform sampler2D texture;
uniform vec2 texOffset;

vec3 rgb2hsv(in vec3 rgb) {
  vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
  vec4 p = mix(vec4(rgb.bg, K.wz), vec4(rgb.gb, K.xy), step(rgb.b, rgb.g));
  vec4 q = mix(vec4(p.xyw, rgb.r), vec4(rgb.r, p.yzx), step(p.x, rgb.r));

  float d = q.x - min(q.w, q.y);
  float e = 1.0e-10;

  return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb( in vec3 hsv ) {
  vec3 rgb = clamp( abs(mod(hsv.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );

  return hsv.z * mix( vec3(1.0), rgb, hsv.y);
}

void main() {
  vec4 orig = texture2D(texture, vertTexCoord.st);
  vec3 hsv = rgb2hsv(orig.rgb);
  
  hsv.x += 2.0 * (u_hue - 0.5); // Add -1 .. +1
  hsv.x = fract(1.0 + hsv.x); // Normalize

  hsv.y += 2.0 * (u_saturation - 0.5); // Add -1 .. +1
  hsv.y = clamp(hsv.y, 0.0, 1.0); // clamp to [0, 1] range

  hsv.z += 2.0 * (u_brightness - 0.5); // Add -1 .. +1
  hsv.z = clamp(hsv.z, 0.0, 1.0); // clamp to [0, 1] range

  gl_FragColor = vec4(hsv2rgb(hsv), u_opacity * orig.a );
}

