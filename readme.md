# ProcessingMiniVJ

![](https://gitlab.com/hamoid/processing-mini-vj/raw/master/screenshot.png)

A simple Processing based projection mapping VJ software.

As Sarah H. and I talked during new year in the country side,
she needs such a program for a workshop.
Apparently there is no cross platform free open source software
she could use in the workshop so I wrote this.

It has 4 layers: 

- background layer (flat color & image)
- bottom video layer
- top video layer
- generative layer (radial shape & grid)

It uses the Keystone library for simple mapping (4 corners).

Processing does not allow for easy toggling full screen, 
so it's configured in the source code.

To show thumbnails in the gui of the video files I used [this approach](https://discourse.processing.org/t/how-to-play-same-gl-video-on-two-windows/2623).

There are two shaders: one for the generative layer which produces a radial
shape or a grid with adjustable parametrs, and a simple HSB shader for the
videos.

The Gui is limited to 768 pixels height for the her laptop.

In the expected use of this tool the result will not be an HD projection,
but projected on soil in an aquarium instead. I suggest lowering the projection resolution to 800x600 in the OS to keep it responsive.

I didn't use openFrameworks because workshop attendees are not coders, so installing an oF
environment and dealing with issues is not doable. Compiling a cross-platform binary is also not doable from my Linux machine.

## Issues

- Sometimes the thumbnail fails to generate when loading an image.
- Sometimes it seems to crash with an opengl problem.
- Sometimes error 1282.
- The Video library version beta4 produces high cpu spikes. beta3 works better.

## Versions

- v1.0 Fri Jan 17th, 2020. Berlin.

